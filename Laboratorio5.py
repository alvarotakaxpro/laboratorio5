#Laboratorio 5

#Haciendo uso de un ciclo anidado FOR simule que: en cada una de las entidades, cada
#persona realiza un pago de cada servicio.

'''entidad=["BN Popular", "BN Nacional", "BN Central de Costa Rica"]
persona=["Juan", "Alberto", "Yessenia"]
servicio=["Servicios Municipales", "Pgo Luz", "Pago Agua"]

for i in entidad:
    for j in persona:
        for e in servicio:
            print("Deposito en {0} de: {1} por concepto de {2}".format(i, j,e))'''

#Ejercicio 2
#Haciendo uso de un ciclo WHILE, realice un menú que tenga 3 Opciones principales y
#cada una de estas opciones tenga dos opciones internas. No es necesario realizar ninguna
#acción, únicamente demostrar el comportamiento de los ciclos correctamente aplicados
#en un menú.
'''while True:
    opcion=int(input("1. Opcion Principal 1 \n2. Opcion Principal 2 \n3. Opcion Principal 3 \n4. salir\nIngrese la Opcion Principal"))
    def secundarias():
        second=input("1. Opcion Secundaria 1\n2. Opcion Secundaria 2\nr. Regresar\n Ingrese opcion secundaria")
        while second.lower()=='r':
            break
        else:
            secundarias()
    while opcion==1:
        secundarias()
        break
    while opcion==2:
        secundarias()
        break
    while opcion==3:
        secundarias()
        break
    while opcion==4:
        print("Muchas Gracias")
        exit()'''

#Ejercicio 3:
#Desarrolle un ejercicio programado, donde ejemplifique el uso de la herencia, el ejemplo
#no puede ser los mismos vistos en clase, debe ser de autoría del estudiante. Este ejemplo
#debe tener las clases, la instancia y el consumo de los atributos y métodos de la clase de
#la cual hereda.
'''class frutas:
    def __init__(self, pCascara, pSabor, pColor):
        self.pCascara=pCascara
        self.pSabor=pSabor
        self.pColor=pColor


class mango(frutas):
    def __init__(self, pDulce, pCitrico, pCascara, pSabor, pColor):
        self.pDulce=pDulce
        self.pCitrico=pCitrico
        super(pCascara, pSabor, pColor)

comida=mango
comida.pCascara="madura"
comida.pSabor="comestible"
comida.pColor="verde y roja"
comida.pDulce="dulce"
comida.pCitrico="El mango es un citrico"
lol=[comida.pCascara, comida.pSabor, comida.pColor, comida.pDulce, comida.pCitrico]

for i in lol:
    print(i)'''

#Ejercicio 4
#El siguiente ejemplo de código contiene errores corríjalos para que el ejemplo se ejecute
#de manera correcta, indique en comentarios cuales fueron los errores encontrados.

'''class Articulo:
    def __init__(self, pcodigo, pnombre):
        self.codigo=pcodigo
        self.nombre=pnombre
    def getNombre(self):
        return self.nombre
    def getColor(self):
        return self.codigo
class Juguete(Articulo):
    def __init__(self, codigo, nombre,precio):
     super().__init__(codigo, nombre)
     self.__precio = precio

    def getDescripcion(self):
      return self.getNombre() + self.__precio + " de color" + self.getColor()


objeto=Juguete("Rojo", "Carrito", "2500")
print(objeto.getDescripcion())
print(objeto.getNombre())
#Observaciones
#-Falta clase Articulo
#-Falta Instanciar
#-falta metodos en Articulo
#-Agregar los datos a cada metodo o atributo'''
